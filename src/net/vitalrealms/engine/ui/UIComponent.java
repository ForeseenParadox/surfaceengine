package net.vitalrealms.engine.ui;

import java.awt.Graphics2D;

import net.vitalrealms.engine.core.Core;
import net.vitalrealms.engine.core.rendering.Render;
import net.vitalrealms.engine.utils.StringUtils;

public abstract class UIComponent
{

	protected String name;
	protected int x;
	protected int y;

	public UIComponent(String name, int x, int y)
	{
		this.name = name;
		this.x = x;
		this.y = y;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getX()
	{
		return x;
	}

	public void setX(int x)
	{
		this.x = x;
	}

	public int getY()
	{
		return y;
	}

	public void setY(int y)
	{
		this.y = y;
	}

	public void renderLeftJustified(String text, int x, int y, int spacing, int right, int bottom)
	{
		Graphics2D g = Core.getInstance().getRenderer().getGraphics();
		int curX = x + spacing;
		int curY = y + spacing;

		for (String word : text.split(" "))
		{

			int wordWidth = StringUtils.getWidthOfString(word, g);
			int wordHeight = StringUtils.getHeightOfString(word, g);

			if ((curX + wordWidth + spacing) >= (x + right))
			{
				curY += (wordHeight + spacing);
				curX = x + spacing;
			}

			if ((curY + wordHeight + spacing) >= (y + bottom))
			{
				break;
			}

			g.drawString(word, curX, curY + (wordHeight + spacing) / 2);

			curX += (wordWidth + spacing);

		}
	}

	public abstract void update();

	public abstract void render(Render render, int translateX, int translateY);

}
