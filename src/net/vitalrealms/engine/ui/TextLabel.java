package net.vitalrealms.engine.ui;

import java.awt.Color;
import java.awt.Font;

import net.vitalrealms.engine.core.rendering.Render;

public class TextLabel extends UIComponent
{

	private String text;
	private Font font;
	private Color color;

	public TextLabel(String name, int x, int y, String text, Font font, Color color)
	{
		super(name, x, y);
		this.text = text;
		this.font = font;
		this.color = color;
	}

	public String getText()
	{
		return text;
	}

	public Font getFont()
	{
		return font;
	}

	public Color getColor()
	{
		return color;
	}

	public void setText(String text)
	{
		this.text = text;
	}

	public void setFont(Font font)
	{
		this.font = font;
	}

	public void setColor(Color color)
	{
		this.color = color;
	}

	@Override
	public void update()
	{

	}

	@Override
	public void render(Render render, int translateX, int translateY)
	{
		render.getGraphics().setColor(color);
		render.getGraphics().setFont(font);
		render.getGraphics().drawString(text, x + translateX, y + translateY);
	}

}
