package net.vitalrealms.engine.ui;

import java.util.ArrayList;
import java.util.List;

import net.vitalrealms.engine.core.rendering.Render;

public class UIManager
{

	private List<UIComponent> uiComponents;

	public UIManager()
	{
		this.uiComponents = new ArrayList<UIComponent>();
	}

	public List<UIComponent> getUIComponents()
	{
		return uiComponents;
	}

	public void addUIComponent(UIComponent component)
	{
		if (component != null)
		{
			this.uiComponents.add(component);
		}
	}

	public void removeUIComponent(UIComponent component)
	{
		if (component != null)
		{
			this.uiComponents.remove(component);
		}
	}

	public void update()
	{
		for (UIComponent comp : uiComponents)
		{
			comp.update();
		}
	}

	public void render(Render render)
	{
		for (UIComponent comp : uiComponents)
		{
			comp.render(render, 0, 0);
		}
	}
}
