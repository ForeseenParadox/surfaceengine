package net.vitalrealms.engine.ui;

import java.awt.image.BufferedImage;

import net.vitalrealms.engine.core.Core;
import net.vitalrealms.engine.core.input.Mouse;
import net.vitalrealms.engine.core.rendering.Render;

public class ImageLabel extends UIComponent
{

	private BufferedImage selectedImage;
	private BufferedImage notSelectedImage;
	private BufferedImage currentImage;
	private boolean mouseOnImage;

	public ImageLabel(String name, int x, int y)
	{
		super(name, x, y);
	}

	public BufferedImage getSelectedImage()
	{
		return selectedImage;
	}

	public BufferedImage getNotSelectedImage()
	{
		return notSelectedImage;
	}

	public BufferedImage getCurrentImage()
	{
		return currentImage;
	}

	public boolean isMouseOnImage()
	{
		return mouseOnImage;
	}

	public void setSelectedImage(BufferedImage selectedImage)
	{
		this.selectedImage = selectedImage;
	}

	public void setNotSelectedImage(BufferedImage notSelectedImage)
	{
		this.notSelectedImage = notSelectedImage;
	}

	@Override
	public void update()
	{

		Mouse m = Core.getInstance().getMouse();

		if ((m.getX() >= x && m.getX() < (x + currentImage.getWidth()))
				&& (m.getY() >= y && m.getY() < (y + currentImage.getHeight())))
		{
			mouseOnImage = true;
		} else
		{
			mouseOnImage = false;
		}

		if (mouseOnImage)
		{
			this.currentImage = selectedImage;
		} else
		{
			this.currentImage = notSelectedImage;
		}

	}

	@Override
	public void render(Render render, int translateX, int translateY)
	{
		render.getGraphics().drawImage(currentImage, x + translateX, y + translateY, null);
	}

}
