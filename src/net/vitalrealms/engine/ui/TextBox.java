package net.vitalrealms.engine.ui;

import java.awt.Color;

import net.vitalrealms.engine.core.rendering.Render;

public class TextBox extends UIComponent
{

	private int width;
	private int height;
	private String text;

	public TextBox(String name, int x, int y)
	{
		super(name, x, y);
	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	public String getText()
	{
		return text;
	}

	public void setText(String text)
	{
		this.text = text;
	}

	public void setWidth(int width)
	{
		this.width = width;
	}

	public void setHeight(int height)
	{
		this.height = height;
	}

	@Override
	public void update()
	{
		// nothing... just a text box
	}

	@Override
	public void render(Render render, int translateX, int translateY)
	{
		render.getGraphics().setColor(Color.GREEN);
		render.getGraphics().fillRect(x, y, width, height);
		render.getGraphics().setColor(Color.RED);
		super.renderLeftJustified(text, x, y, 5, width, height);
	}

}
