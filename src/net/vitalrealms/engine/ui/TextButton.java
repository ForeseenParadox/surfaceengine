package net.vitalrealms.engine.ui;

import java.awt.Font;
import java.awt.Graphics;

import net.vitalrealms.engine.core.Core;
import net.vitalrealms.engine.core.input.Mouse;
import net.vitalrealms.engine.core.rendering.Render;
import net.vitalrealms.engine.utils.StringUtils;

public class TextButton extends UIComponent
{

	private Font hoveredFont;
	private Font notHoveredFont;
	private Font currentFont;
	private boolean hovered;

	public TextButton(String name, int x, int y, Font hoveredFont, Font notHoveredFont)
	{
		super(name, x, y);
		this.hoveredFont = hoveredFont;
		this.notHoveredFont = notHoveredFont;
		this.currentFont = notHoveredFont;
	}

	public Font getHoveredFont()
	{
		return hoveredFont;
	}

	public Font getNotHoveredFont()
	{
		return notHoveredFont;
	}

	public Font getCurrentFont()
	{
		return currentFont;
	}

	public boolean isHovered()
	{
		return hovered;
	}

	public void setHoveredFont(Font hoveredFont)
	{
		this.hoveredFont = hoveredFont;
	}

	public void setNotHoveredFont(Font notHoveredFont)
	{
		this.notHoveredFont = notHoveredFont;
	}

	@Override
	public void update()
	{

		Mouse m = Core.getInstance().getMouse();
		Graphics g = Core.getInstance().getRenderer().getGraphics();

		int width = StringUtils.getWidthOfString(name, g);
		int height = StringUtils.getHeightOfString(name, g);
		System.out.println(m.getY() + " " + y + " " + (y + height));

		if ((m.getX() >= x) && (m.getX() < (x + width)) && (m.getY() >= (y - height)) && (m.getY() < y))
		{

			hovered = true;
			currentFont = hoveredFont;
		} else
		{
			hovered = false;
			currentFont = notHoveredFont;
		}

		if (!m.isVisible())
		{
			hovered = false;
			currentFont = notHoveredFont;
		}
	}

	@Override
	public void render(Render render, int translateX, int translateY)
	{
		render.getGraphics().setFont(currentFont);
		render.getGraphics().drawString(name, x, y);
	}
}
