package net.vitalrealms.engine.ui.menu;

import java.awt.Color;
import java.awt.Font;

public class TextMenuSelection
{

	private String name;
	private Font selectedFont;
	private Font notSelectedFont;
	private Color selectedColor;
	private Color notSelectedColor;

	public TextMenuSelection(String name, Font selectedFont, Font notSelectedFont, Color selectedColor, Color notSelectedColor)
	{
		this.name = name;
		this.selectedFont = selectedFont;
		this.notSelectedFont = notSelectedFont;
		this.selectedColor = selectedColor;
		this.notSelectedColor = notSelectedColor;
	}

	public String getName()
	{
		return name;
	}

	public Font getSelectedFont()
	{
		return selectedFont;
	}

	public Font getNotSelectedFont()
	{
		return notSelectedFont;
	}

	public Color getSelectedColor()
	{
		return selectedColor;
	}

	public Color getNotSelectedColor()
	{
		return notSelectedColor;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setSelectedFont(Font selectedFont)
	{
		this.selectedFont = selectedFont;
	}

	public void setNotSelectedFont(Font notSelectedFont)
	{
		this.notSelectedFont = notSelectedFont;
	}

	public void setSelectedColor(Color selectedColor)
	{
		this.selectedColor = selectedColor;
	}

	public void setNotSelectedColor(Color notSelectedColor)
	{
		this.notSelectedColor = notSelectedColor;
	}
}
