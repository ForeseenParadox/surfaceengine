package net.vitalrealms.engine.ui.menu;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import net.vitalrealms.engine.core.Core;
import net.vitalrealms.engine.core.input.Keyboard;
import net.vitalrealms.engine.core.rendering.Render;
import net.vitalrealms.engine.ui.ImageLabel;
import net.vitalrealms.engine.ui.UIComponent;
import net.vitalrealms.engine.utils.Delay;

public class ImageMenu extends UIComponent
{

	private List<ImageLabel> imageLabels;
	private int spacing;
	private int selectedItem;
	private MenuLayout menuLayout;
	private Delay scrollCooldown;

	public ImageMenu(String name, int x, int y)
	{
		super(name, x, y);
		this.imageLabels = new ArrayList<ImageLabel>();
		this.scrollCooldown = new Delay(120);
	}

	public List<ImageLabel> getImageLabels()
	{
		return imageLabels;
	}

	public int getSpacing()
	{
		return spacing;
	}

	public int getSelected()
	{
		return selectedItem;
	}

	public MenuLayout getMenuLayout()
	{
		return menuLayout;
	}

	public Delay getScrollCooldown()
	{
		return scrollCooldown;
	}

	public void setSpacing(int spacing)
	{
		this.spacing = spacing;
	}

	public void setSelected(int selected)
	{
		this.selectedItem = selected;
	}

	public void setMenuLayout(MenuLayout menuLayout)
	{
		this.menuLayout = menuLayout;
	}

	public void setScrollCooldown(Delay scrollCooldown)
	{
		this.scrollCooldown = scrollCooldown;
	}

	@Override
	public void update()
	{
		Keyboard kb = Core.getInstance().getKeyboard();

		if (scrollCooldown.isReady())
		{

			if (kb.isKeyPressed(KeyEvent.VK_UP))
			{
				if (selectedItem > 0)
				{
					selectedItem--;
				} else
				{
					selectedItem = imageLabels.size() - 1;
				}
			} else if (kb.isKeyPressed(KeyEvent.VK_DOWN))
			{
				selectedItem++;
				if (selectedItem >= imageLabels.size())
				{
					selectedItem = 0;
				}
			}
		}
	}

	@Override
	public void render(Render render, int translateX, int translateY)
	{

		Graphics2D g = Core.getInstance().getRenderer().getGraphics();

		int curX = x;
		int curY = y;

		for (int i = 0; i < imageLabels.size(); i++)
		{

			ImageLabel label = imageLabels.get(i);

			BufferedImage img = null;

			// set font and color
			if (selectedItem == i)
			{
				img = label.getSelectedImage();
			} else
			{
				img = label.getNotSelectedImage();
			}

			g.drawImage(img, curX, curY, null);

			// check for new line
			int height = img.getHeight();
			int width = img.getWidth();

			if (menuLayout == MenuLayout.VERTICLE)
			{

				if (curY + height + spacing >= Core.getInstance().getHeight())
				{
					curX += spacing + width;
					curY = y;
				} else
				{
					curY += spacing + height;
				}

			} else if (menuLayout == MenuLayout.HORIZONTAL)
			{

				if (curX + width + spacing >= Core.getInstance().getWidth())
				{
					curX = x;
					curY += spacing + height;
				} else
				{
					curX += spacing + width;
				}
			}

		}

	}

}
