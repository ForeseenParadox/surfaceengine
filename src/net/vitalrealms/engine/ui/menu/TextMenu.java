package net.vitalrealms.engine.ui.menu;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import net.vitalrealms.engine.core.Core;
import net.vitalrealms.engine.core.input.Keyboard;
import net.vitalrealms.engine.core.rendering.Render;
import net.vitalrealms.engine.ui.UIComponent;
import net.vitalrealms.engine.utils.StringUtils;

public class TextMenu extends UIComponent
{

	/*
	 * Implement blipping sounds and stuff like that later
	 */

	private List<TextMenuSelection> selections;
	private int selectedItem;
	private int spacing;
	private MenuLayout menuLayout;

	public TextMenu(String name, int x, int y, int spacing)
	{
		super(name, x, y);
		this.selections = new ArrayList<TextMenuSelection>();
		this.selectedItem = 0;
		this.spacing = spacing;
		this.menuLayout = MenuLayout.VERTICLE;
	}

	public List<TextMenuSelection> getSelections()
	{
		return selections;
	}

	public int getSelectedItem()
	{
		return selectedItem;
	}

	public int getSpacing()
	{
		return spacing;
	}

	public MenuLayout getMenuLayout()
	{
		return menuLayout;
	}

	public void addMenuSelection(TextMenuSelection selection)
	{
		if (selection != null)
		{
			this.selections.add(selection);
		}
	}

	public void removeMenuSelection(TextMenuSelection selection)
	{
		if (selection != null)
		{
			this.selections.remove(selection);
		}
	}

	public void setSelected(int selected)
	{
		if (selected >= 0 && selected < selections.size())
		{
			this.selectedItem = selected;
		}
	}

	public void setSelected(String selectionName)
	{
		for (TextMenuSelection selection : selections)
		{
			if (selection.getName().equalsIgnoreCase(selectionName))
			{
				this.selectedItem = this.selections.indexOf(selection);
			}
		}
	}

	public void setSpacing(int spacing)
	{
		this.spacing = spacing;
	}

	public void setMenuLayout(MenuLayout menuLayout)
	{
		this.menuLayout = menuLayout;
	}

	@Override
	public void update()
	{

		Keyboard kb = Core.getInstance().getKeyboard();

		if (kb.wasKeyPressed(KeyEvent.VK_UP))
		{
			if (selectedItem > 0)
			{
				selectedItem--;
			} else
			{
				selectedItem = selections.size() - 1;
			}
		} else if (kb.wasKeyPressed(KeyEvent.VK_DOWN))
		{
			selectedItem++;
			if (selectedItem >= selections.size())
			{
				selectedItem = 0;
			}
		}
	}

	@Override
	public void render(Render render, int translateX, int translateY)
	{

		Graphics2D g = Core.getInstance().getRenderer().getGraphics();

		int curX = x;
		int curY = y;

		for (int i = 0; i < selections.size(); i++)
		{

			TextMenuSelection sel = selections.get(i);

			// set font and color
			if (selectedItem == i)
			{
				g.setColor(sel.getSelectedColor());
				g.setFont(sel.getSelectedFont());
			} else
			{
				g.setColor(sel.getNotSelectedColor());
				g.setFont(sel.getNotSelectedFont());
			}

			g.drawString(sel.getName(), curX, curY);

			// reset font so it doesn't cause menu glitch
			g.setFont(sel.getNotSelectedFont());

			// check for new line
			int height = StringUtils.getHeightOfString(sel.getName(), g);
			int width = StringUtils.getWidthOfString(sel.getName(), g);

			if (menuLayout == MenuLayout.VERTICLE)
			{

				if (curY + height + spacing >= Core.getInstance().getHeight())
				{
					curX += spacing + width;
					curY = y;
				} else
				{
					curY += spacing + height;
				}

			} else if (menuLayout == MenuLayout.HORIZONTAL)
			{

				if (curX + width + spacing >= Core.getInstance().getWidth())
				{
					curX = x;
					curY += spacing + height;
				} else
				{
					curX += spacing + width;
				}
			}

		}

		/*
		 * Font selectedFont = this.font.deriveFont((float) this.font.getSize()
		 * + 10); Font unSelectedFont = this.font;
		 * 
		 * int y = super.y; for (TextMenuSelection selection : selections) { y
		 * += vSpacing;
		 * 
		 * if
		 * (selection.getName().equalsIgnoreCase(selections.get(selectedItem).
		 * getName())) { render.getGraphics().setFont(selectedFont);
		 * render.getGraphics().setColor(selection.getSelected()); } else {
		 * render.getGraphics().setFont(unSelectedFont);
		 * render.getGraphics().setColor(selection.getNotSelected()); }
		 * 
		 * render.getGraphics().drawString(selection.getName(), super.x +
		 * translateX, y + translateY); }
		 */
	}
}
