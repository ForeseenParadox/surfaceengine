package net.vitalrealms.engine.ui;

import java.awt.image.BufferedImage;

import net.vitalrealms.engine.core.Core;
import net.vitalrealms.engine.core.input.Mouse;
import net.vitalrealms.engine.core.rendering.Render;

public class ImageButton extends UIComponent
{

	private BufferedImage hoveredImage;
	private BufferedImage notHoveredImage;
	private BufferedImage currentImage;
	private boolean hovered;

	public ImageButton(String name, int x, int y, BufferedImage hoveredImage, BufferedImage notHoveredImage)
	{
		super(name, x, y);
		this.hoveredImage = hoveredImage;
		this.notHoveredImage = notHoveredImage;
		this.currentImage = notHoveredImage;
	}

	public BufferedImage getHoveredImage()
	{
		return hoveredImage;
	}

	public BufferedImage getNotHoveredImage()
	{
		return notHoveredImage;
	}

	public boolean isHovered()
	{
		return hovered;
	}

	public void setHoveredImage(BufferedImage hoveredImage)
	{
		this.hoveredImage = hoveredImage;
	}

	public void setNotHoveredImage(BufferedImage notHoveredImage)
	{
		this.notHoveredImage = notHoveredImage;
	}

	@Override
	public void update()
	{

		Mouse m = Core.getInstance().getMouse();

		if ((m.getX() <= x) && (m.getX() <= (x + currentImage.getWidth())) && (m.getY() <= y) && (m.getY() <= (y + currentImage.getHeight())))
		{
			hovered = true;
			currentImage = hoveredImage;
		} else
		{
			hovered = false;
			currentImage = notHoveredImage;
		}
	}

	@Override
	public void render(Render render, int translateX, int translateY)
	{
		render.getGraphics().drawImage(currentImage, x + translateX, y + translateY, null);
	}
}
