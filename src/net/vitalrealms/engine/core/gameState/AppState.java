package net.vitalrealms.engine.core.gameState;

/**
 * A class representing an application state.
 * 
 * @author ForeseenParadox
 * 
 */
public abstract class AppState
{

	/**
	 * Constructs a new AppState.
	 */
	public AppState()
	{

	}

	/**
	 * Initializes this state.
	 */
	public void init()
	{

	}

	/**
	 * Responds to a resize event.
	 */
	public void resize()
	{

	}

	/**
	 * Updates this state.
	 */
	public void update()
	{

	}

	/**
	 * Renders this state.
	 */
	public void render()
	{

	}

	/**
	 * Disposes this state.
	 */
	public void dispose()
	{

	}

}
