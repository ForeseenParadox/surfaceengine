package net.vitalrealms.engine.core.gameState;

import java.util.ArrayList;
import java.util.List;

public class AppStateManager
{

	private List<AppState> gameStates;
	private AppState currentState;

	public AppStateManager()
	{
		this.gameStates = new ArrayList<AppState>();
	}

	public List<AppState> getGameStates()
	{
		return gameStates;
	}

	public AppState getCurrentState()
	{
		return currentState;
	}

	public void addGameState(AppState state)
	{
		if (state != null)
		{
			this.gameStates.add(state);
		}
	}

	public void removeGameState(AppState state)
	{
		if (state != null)
		{
			this.gameStates.remove(state);
		}
	}

	public void setCurrentGameState(AppState currentState)
	{
		if (currentState != null)
		{
			this.currentState = currentState;
		}
	}

}
