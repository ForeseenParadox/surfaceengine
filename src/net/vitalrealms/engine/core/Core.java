package net.vitalrealms.engine.core;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;

import net.vitalrealms.engine.core.input.Keyboard;
import net.vitalrealms.engine.core.input.Mouse;
import net.vitalrealms.engine.core.rendering.Render;

/**
 * This class represents the core of the engine. It ties together all engine
 * subcomponents. This class is a singleton to prevent multiple instances
 * floating around. Use the static getInstance method to access instance fields.
 * 
 * @author ForeseenParadox
 */
public class Core implements Runnable
{

	/**
	 * The amount of buffers that the engine will attempt to use for rendering.
	 * The default is double buffered.
	 */
	public static final int NUM_BUFFERS = 2;

	/**
	 * The singleton instance of the Core class.
	 */
	private static final Core INSTANCE = new Core();

	/**
	 * Boolean flag representing whether the engine is actively executing.
	 */
	private boolean running;

	/**
	 * The thread in which the application is executed on.
	 */
	private Thread gameThread;

	/**
	 * The FPS in which the engine will attempt to execute at.
	 */
	private int targetFps = 60;

	/**
	 * The UPS in which the engine will attempt to execute at.
	 */
	private int targetUps = 60;

	/**
	 * The amount of FPS that the application is currently executing at.
	 */
	private int fps;

	/**
	 * The amount of UPS that the application is currently executing at.
	 */
	private int ups;

	/**
	 * The amount of memory that has been consumed by the application during
	 * runtime.
	 */
	private long usedMemory;

	/**
	 * The amount of memory that is free to the JVM during runtime.
	 */
	private long freeMemory;

	/**
	 * The total amount of memory that the engine will attempt to consume at a
	 * given time.
	 */
	private long totalMemory;

	/**
	 * Internal boolean flag that details whether or not the internal timings of
	 * the application loop should be changed because a certain method was
	 * invoked that would cause so.
	 */
	private boolean updateTimer;

	/**
	 * A reference to the application being executed.
	 */
	private Application app;

	/**
	 * An instance of JFrame used for the windowing engine.
	 */
	private JFrame window;

	/**
	 * An instance of Canvas for rendering all of the applications graphics.
	 */
	private Canvas canvas;

	/**
	 * An instance of the Render class to render graphics to the canvas.
	 */
	private Render render;

	/**
	 * The buffer strategy that the canvas is using to render graphics.
	 */
	private BufferStrategy buffer;

	/**
	 * A graphics reference of the buffer strategies graphics to render the
	 * final rendered image.
	 */
	private Graphics g;

	/**
	 * An instance of the Keyboard class to handle keyboard input events.
	 */
	private Keyboard kb;

	/**
	 * An instance of the Mouse class to handle mouse input events.
	 */
	private Mouse mouse;

	/**
	 * An instance of a splash screen animation to animate the splash screens
	 * that will display before the actual application loop starts.
	 */
	private SplashScreenAnimation ssMan;

	/*
	 * A private constructor to enforce that this class is a singleton.
	 */
	private Core()
	{
		this.running = false;
		this.gameThread = new Thread(this, "Game-Thread");
		this.window = new JFrame();
		this.canvas = new Canvas();
		this.ssMan = new SplashScreenAnimation();
	}

	/**
	 * Gets the singleton instance of the Core class.
	 */
	public static Core getInstance()
	{
		return INSTANCE;
	}

	/**
	 * Returns a boolean result reresenting if the application loop is currently
	 * executing.
	 */
	public boolean isRunning()
	{
		return running;
	}

	/**
	 * Gets the FPS in which the engine will attempt to execute at.
	 */
	public int getTargetFps()
	{
		return targetFps;
	}

	/**
	 * Gets the UPS in which the engine will attempt to execute at.
	 */
	public int getTargetUps()
	{
		return targetUps;
	}

	/**
	 * Gets the FPS that the application is currently executing at.
	 */
	public int getFps()
	{
		return fps;
	}

	/**
	 * Gets the UPS that the application is currently executing at.
	 */
	public int getUps()
	{
		return ups;
	}

	/**
	 * Gets the amount of memory that the application is free to consume.
	 */
	public long getFreeMemory()
	{
		return freeMemory;
	}

	/**
	 * Gets the amount of memory that the application has used.
	 */
	public long getUsedMemory()
	{
		return usedMemory;
	}

	/**
	 * Gets the total amount of memory that the engine will attempt to use at
	 * any given point.
	 */
	public long getTotalMemory()
	{
		return totalMemory;
	}

	/**
	 * Gets the reference to the application being executed.
	 */
	public Application getApp()
	{
		return app;
	}

	/**
	 * Returns the canvas where the application renders the graphics to.
	 */
	public Canvas getCanvas()
	{
		return canvas;
	}

	/**
	 * Gets the renderer used to rasterize graphics to the screen.
	 */
	public Render getRenderer()
	{
		return render;
	}

	/**
	 * Gets the title of the window.
	 */
	public String getTitle()
	{
		return window.getTitle();
	}

	/**
	 * Gets the width of the window.
	 */
	public int getWidth()
	{
		return canvas.getWidth();
	}

	/**
	 * Gets the height of the window.
	 */
	public int getHeight()
	{
		return canvas.getHeight();
	}

	/**
	 * Gets if the window is resizable or not.
	 */
	public boolean isResizable()
	{
		return window.isResizable();
	}

	/**
	 * Gets the keyboard instance used to handle keyboard input events.
	 */
	public Keyboard getKeyboard()
	{
		return kb;
	}

	/**
	 * Gets the mouse instance used to handle mouse input events.
	 */
	public Mouse getMouse()
	{
		return mouse;
	}

	/**
	 * Gets the SplashScreenManager. Here, you can add images to be displayed
	 * before the main application loop executes.
	 */
	public SplashScreenAnimation getSplashScreenManager()
	{
		return ssMan;
	}

	/**
	 * Sets the FPS that the engine will attempt to execute at.
	 */
	public void setTargetFps(int targetFps)
	{
		this.targetFps = targetFps;
		updateTimer = true;
	}

	/**
	 * Sets the UPS that the engine will attempt to execute at.
	 */
	public void setTargetUps(int targetUps)
	{
		this.targetUps = targetUps;
		updateTimer = true;
	}

	/**
	 * Sets the application reference that will be executed.
	 */
	public void setApplication(Application app)
	{
		this.app = app;
	}

	/**
	 * Sets the title of the window.
	 */
	public void setTitle(String title)
	{
		window.setTitle(title);
	}

	/**
	 * Sets the size of the window.
	 */
	public void setSize(int width, int height)
	{
		canvas.setSize(width, height);
		window.pack();
	}

	/**
	 * Sets if the window is resizable.
	 */
	public void setResizable(boolean resizable)
	{
		window.setResizable(resizable);
	}

	/**
	 * Starts the execution of the main application loop. If there are any
	 * splash screens registered, they will be displayed first.
	 */
	public void start()
	{
		running = true;
		ssMan.start();
		gameThread.start();
	}

	/**
	 * Stops the execution of the engine.
	 */
	public void stop()
	{
		running = false;
		ssMan.stop();
	}

	/**
	 * Runs the main application loop.
	 */
	public void run()
	{
		init();
		double maxFrameDelay = 1e9 / targetFps;
		double maxUpdateDelay = 1e9 / targetUps;
		long delta = 0;
		long lastFrame = System.nanoTime();
		double lastRender = 0;
		double lastUpdate = 0;
		int frames = 0;
		int updates = 0;
		long timer = System.currentTimeMillis();

		/*
		 * game loop
		 */
		while (running)
		{

			long thisFrame = System.nanoTime();
			delta = (thisFrame - lastFrame);
			lastFrame = thisFrame;

			lastRender += delta;
			lastUpdate += delta;

			if (updateTimer)
			{
				maxFrameDelay = 1e9 / targetFps;
				maxUpdateDelay = 1e9 / targetUps;
			}

			if (targetUps >= 0)
			{
				while (lastUpdate >= maxUpdateDelay)
				{
					lastUpdate -= maxUpdateDelay;
					update();
					updates++;
				}
			} else
			{
				lastUpdate = 0;
				update();
				updates++;
			}

			if (targetFps >= 0)
			{
				if (lastRender >= maxFrameDelay)
				{

					lastRender -= maxFrameDelay;
					render();
					frames++;
				} else if (lastRender <= (25 * 1000000))
				{
					try
					{
						Thread.sleep(1);
					} catch (InterruptedException e)
					{
						e.printStackTrace();
					}
				}
			} else
			{
				lastRender = 0;
				render();
				frames++;
			}

			if (System.currentTimeMillis() - timer >= 1000)
			{
				timer += 1000;
				fps = frames;
				ups = updates;
				frames = 0;
				updates = 0;
			}

		}
		dispose();
	}

	/**
	 * Initializes the engine and all of the application's system resources.
	 */
	private void init()
	{

		/*
		 * init window
		 */

		window.add(canvas);
		window.pack();
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setLocationRelativeTo(null);
		window.setVisible(true);

		render = new Render();

		canvas.createBufferStrategy(NUM_BUFFERS);
		buffer = canvas.getBufferStrategy();
		g = buffer.getDrawGraphics();

		kb = new Keyboard();
		mouse = new Mouse(this);

		// init app
		if (app != null)
		{
			app.init();
		}

	}

	/**
	 * Updates the engine and application once.
	 */
	private void update()
	{

		kb.update();
		mouse.update();

		if (ssMan.isRunning())
		{
			ssMan.update();
		} else
		{
			if (app != null)
			{
				app.update();
			}
		}

		// update mem statistics
		freeMemory = Runtime.getRuntime().freeMemory();
		totalMemory = Runtime.getRuntime().totalMemory();
		usedMemory = totalMemory - freeMemory;

	}

	/**
	 * Renders the application once.
	 */
	private void render()
	{

		buffer = canvas.getBufferStrategy();
		g = buffer.getDrawGraphics();

		// update and clear the screen
		render.update();
		render.clear(Color.BLACK);

		if (ssMan.isRunning())
		{
			ssMan.render();
		} else
		{
			if (app != null)
			{
				app.render();
			}
		}

		render.render(g);

		// next frame
		g.dispose();
		buffer.show();

	}

	/**
	 * Disposes the engine and all of the application's system resources.
	 */
	private void dispose()
	{
		if (app != null)
		{
			app.dispose();
		}
	}

}
