package net.vitalrealms.engine.core;

import net.vitalrealms.engine.core.gameState.AppStateManager;

/**
 * Defines a common behavior for all applications.
 * 
 * @author ForeseenParadox
 */
public abstract class Application
{

	/*
	 * A reference to the singleton of the Core class.
	 */
	public static final Core ENGINE = Core.getInstance();

	/**
	 * An instance of AppStateManager used to manager application states.
	 */
	protected AppStateManager appStateManager;

	/**
	 * Constructs an application.
	 */
	public Application()
	{
		appStateManager = new AppStateManager();
	}

	/**
	 * Returns the AppStateManager, which is used for managing the state of the
	 * application.
	 */
	public AppStateManager getGameStateManager()
	{
		return appStateManager;
	}

	/**
	 * Invoked when the engine is initializing. This is the stage of the engine
	 * pipeline where the application should be initialized. This includes
	 * loading all resources and instantiating all objects relative to the
	 * engine.
	 */
	public void init()
	{

	}

	/**
	 * Invoked when the application windows is resized. This should handle all
	 * events relative to window size.
	 */
	public void resize()
	{

	}

	/**
	 * Updates the application once. This method should only be used to change
	 * the application state, not for rendering. That is what the rendering
	 * method is for as these two methods both have different timing.
	 */
	public void update()
	{

	}

	/**
	 * Renders the game once. This method should only be used for rendering and
	 * not updating because these two methods have different timing.
	 */
	public void render()
	{

	}

	/**
	 * Invoked the the engine is disposing resources and shutting down. This is
	 * where you should release any system resources relative to the engine. Do
	 * not use a finalize method.
	 */
	public void dispose()
	{

	}

}
