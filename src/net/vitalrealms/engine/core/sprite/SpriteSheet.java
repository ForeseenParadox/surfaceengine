package net.vitalrealms.engine.core.sprite;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;

import javax.imageio.ImageIO;

public class SpriteSheet {

	private int width;
	private int height;
	private int spriteWidth;
	private int spriteHeight;
	private BufferedImage image;
	private int[] pixelData;

	public SpriteSheet(String file, int spriteWidth, int spriteHeight) {
		this(new File(file), spriteWidth, spriteHeight);
	}

	public SpriteSheet(File file, int spriteWidth, int spriteHeight) {
		try {
			if (!file.exists()) {
				throw new FileNotFoundException("The sprite sheet must exist!");
			} else {
				image = ImageIO.read(file);
				this.width = image.getWidth();
				this.height = image.getHeight();
				this.spriteWidth = spriteWidth;
				this.spriteHeight = spriteHeight;
				this.pixelData = image.getRGB(0, 0, width, height, null, 0,
						width);

				for (int i = 0; i < pixelData.length; i++) {

					if (pixelData[i] == Sprite.TRANSPARENT) {
						pixelData[i] = 0x00000000;
					}
				}

				image = new BufferedImage(width, height,
						BufferedImage.TYPE_INT_ARGB);
				this.image.setRGB(0, 0, width, height, pixelData, 0, width);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getSpriteWidth() {
		return spriteWidth;
	}

	public int getSpriteHeight() {
		return spriteHeight;
	}

	public BufferedImage getImage() {
		return image;
	}

	public int[] getPixelData() {
		return pixelData;
	}

	public Sprite loadSprite(int x, int y, int width, int height) {
		return new Sprite(image.getSubimage(x * spriteWidth, y * spriteHeight,
				width * spriteWidth, height * spriteHeight));
	}
}
