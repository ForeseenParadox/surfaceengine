package net.vitalrealms.engine.core.sprite;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;

import javax.imageio.ImageIO;

public class Sprite {

	public static final int TRANSPARENT = 0xFFFF00DC;

	private int width;
	private int height;
	private int[] pixelData;
	private BufferedImage image;

	public Sprite(String path) {
		this(new File(path));
	}

	public Sprite(File file) {
		try {
			if (!file.exists()) {
				throw new FileNotFoundException(
						"File must exist to load sprite");
			} else {
				image = ImageIO.read(file);
				this.width = image.getWidth();
				this.height = image.getHeight();
				pixelData = image.getRGB(0, 0, width, height, null, 0, width);

				for (int i = 0; i < pixelData.length; i++) {
					if (pixelData[i] == TRANSPARENT) {
						pixelData[i] = 0x00000000;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Sprite(int width, int height, int rgb) {

		this.width = width;
		this.height = height;

		pixelData = new int[width * height];

		for (int i = 0; i < width * height; i++) {
			pixelData[i] = rgb;
		}

		this.image = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);
		image.setRGB(0, 0, width, height, pixelData, 0, width);

	}

	public Sprite(BufferedImage image) {
		this.width = image.getWidth();
		this.height = image.getHeight();
		this.image = image;
		this.pixelData = image.getRGB(0, 0, width, height, null, 0, width);
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public BufferedImage getRenderedImage() {
		return image;
	}

	public int[] getPixelData() {
		return pixelData;
	}

}
