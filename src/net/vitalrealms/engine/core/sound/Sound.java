package net.vitalrealms.engine.core.sound;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Sound
{

	private Clip clip;

	public Sound(String path)
	{
		this(new File(path));
	}

	public Sound(File file)
	{
		try
		{
			this.clip = AudioSystem.getClip();

			clip.open(AudioSystem.getAudioInputStream(new BufferedInputStream(new FileInputStream(file))));
		} catch (Exception e)
		{
			System.err.println("Error loading sound: ");
			e.printStackTrace();
		}
	}

	public void start()
	{
		if (clip.isRunning())
		{
			reset();
		}
		clip.start();
	}

	public void reset()
	{
		clip.setFramePosition(0);
	}

	public void loop(int amount)
	{
		if (amount == -1)
		{
			clip.loop(Clip.LOOP_CONTINUOUSLY);
		} else
		{
			clip.loop(amount);
		}
	}

	public void close()
	{
		clip.close();
	}

	public void stop()
	{
		clip.stop();
	}

}
