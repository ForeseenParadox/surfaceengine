package net.vitalrealms.engine.core.animation;

import java.util.ArrayList;
import java.util.List;

import net.vitalrealms.engine.core.sprite.Sprite;
import net.vitalrealms.engine.utils.Delay;

/**
 * A class representing a sprite animation.
 * 
 * @author ForeseenParadox
 */
public class SpriteAnimation
{

	/**
	 * Boolean value stating if the animation is running.
	 */
	private boolean running = false;

	/**
	 * A list of frames for the animations.
	 */
	private List<Sprite> frames;

	/**
	 * The current frame index.
	 */
	private int frame;

	/**
	 * The delay instance for switching between frames in this animation.
	 */
	private Delay animDelay;

	/**
	 * Constructs a sprite animation with the specified delay.
	 */
	public SpriteAnimation(long delta)
	{
		frames = new ArrayList<Sprite>();
		this.frame = 0;
		this.animDelay = new Delay(delta);
	}

	/**
	 * Returns if this animation is running.
	 */
	public boolean isRunning()
	{
		return running;
	}

	/**
	 * Gets the list of frames of this sprite animation.
	 */
	public List<Sprite> getFrames()
	{
		return frames;
	}

	/**
	 * Gets the current frame.
	 */
	public Sprite getFrame()
	{
		if (running)
		{
			return frames.get(frame);
		} else
		{
			return frames.get(0);
		}
	}

	/**
	 * Adds a frame to the animation.
	 */
	public void addFrame(Sprite frame)
	{
		this.frames.add(frame);
	}

	/**
	 * Removes a frame from the animation.
	 */
	public void removeFrame(Sprite frame)
	{
		this.frames.remove(frame);
	}

	/**
	 * Starts the animation.
	 */
	public void start()
	{
		running = true;
	}

	/**
	 * Resets the animation
	 */
	public void reset()
	{
		frame = 0;
	}

	/**
	 * Stops the animation on the current frame. If resumes(play invoked), it
	 * will start at the same location. Call reset to start over.
	 */
	public void stop()
	{
		running = false;
	}

	/**
	 * Updates the animation.
	 */
	public void update()
	{
		// update timer

		if (animDelay.isReady())
		{
			frame++;
			if (frame >= getFrames().size())
			{
				reset();
			}
		}
	}
}
