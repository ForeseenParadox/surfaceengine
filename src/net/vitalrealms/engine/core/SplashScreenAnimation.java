package net.vitalrealms.engine.core;

import java.util.ArrayList;
import java.util.List;

import net.vitalrealms.engine.utils.Delay;
import net.vitalrealms.engine.utils.Timer;

/**
 * A class used to manage splash screens.
 * 
 * @author ForeseenParadox
 */
public class SplashScreenAnimation
{

	/**
	 * A boolean flag that stores if the splash screens are currently being
	 * displayed.
	 */
	private boolean running;

	/**
	 * A list of splash screens to be displayed.
	 */
	private List<SplashScreen> splashScreens;

	/**
	 * The integer index of the current splash screen.
	 */
	private int ssIndex;

	/**
	 * The delay for the current splash screen.
	 */
	private Delay ssDelay;

	/**
	 * The timer until the current splash screen starts to fade.
	 */
	private Timer fadeTimer;

	/**
	 * Constructs a new SplashScreenManager.
	 */
	public SplashScreenAnimation()
	{
		this.running = false;
		this.splashScreens = new ArrayList<SplashScreen>();
		this.ssIndex = 0;
		this.ssDelay = new Delay(0);
		this.fadeTimer = new Timer();
	}

	/**
	 * Returns the boolean value stating if the splash screens are currently
	 * being displayed.
	 */
	public boolean isRunning()
	{
		return running;
	}

	/**
	 * Returns a list of splash screens that will/have been displayed.
	 */
	public List<SplashScreen> getSplashScreens()
	{
		return splashScreens;
	}

	/**
	 * Returns the current SplashScreen instance.
	 */
	public SplashScreen getCurrentSplashScreen()
	{
		return splashScreens.get(ssIndex);
	}

	/**
	 * Starts the splash screen animation.
	 */
	public void start()
	{
		if (this.splashScreens.size() > 0)
		{
			this.running = true;
			this.ssIndex = 0;
			this.ssDelay.setDelay(getCurrentSplashScreen().getLength());
			this.fadeTimer.reset();
		} else
		{
			this.running = false;
		}
	}

	/**
	 * Stops the splash screen animation.
	 */
	public void stop()
	{
		this.running = false;
	}

	/**
	 * Adds the specified splash screen to the list of splash screens to be
	 * displayed.
	 */
	public void addSplashScreen(SplashScreen splashScreen)
	{
		if (splashScreen != null)
		{
			splashScreens.add(splashScreen);
		}
	}

	/**
	 * Removes the specified splash screen from the list of splash screens to be
	 * displayed.
	 */
	public void removeSplashScreen(SplashScreen splashScreen)
	{
		if (splashScreen != null)
		{
			splashScreens.remove(splashScreen);
		}
	}

	/**
	 * Updates the splash screen animation.
	 */
	public void update()
	{
		if (ssDelay.isReady())
		{
			ssIndex++;
			this.fadeTimer.reset();
			if (!(ssIndex >= splashScreens.size()))
			{
				ssDelay.setDelay(this.getCurrentSplashScreen().getLength());
			} else
			{
				this.running = false;
			}
		}

	}

	/**
	 * Renders the splash screen animation.
	 */
	public void render()
	{

		if (running)
		{

			fadeTimer.update();

			int fade = 0;

			int fadeLength = (int) (getCurrentSplashScreen().getLength() - getCurrentSplashScreen().getFadeTime());

			int deltaLength = (int) ((fadeTimer.getElapsed() - getCurrentSplashScreen().getLength()) + fadeLength);

			if (fadeTimer.getElapsed() >= getCurrentSplashScreen().getFadeTime())
			{
				fade = (int) ((255 * deltaLength) / fadeLength);
			} else
			{
				fade = 0;
			}

			if (fade > 255)
			{
				fade = 255;
			}

			this.splashScreens.get(ssIndex).render(fade);
		}
	}
}
