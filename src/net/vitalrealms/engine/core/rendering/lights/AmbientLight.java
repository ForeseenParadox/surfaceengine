package net.vitalrealms.engine.core.rendering.lights;

public class AmbientLight
{

	private int intensity;
	private int color;

	public AmbientLight(int intensity, int color)
	{
		this.intensity = intensity;
		this.color = color;
	}

	public int getIntensity()
	{
		return intensity;
	}

	public int getColor()
	{
		return color;
	}

	public void setIntensity(int intensity)
	{
		this.intensity = intensity;
	}

	public void setColor(int color)
	{
		this.color = color;
	}
}
