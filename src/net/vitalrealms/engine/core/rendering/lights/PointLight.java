package net.vitalrealms.engine.core.rendering.lights;

public class PointLight
{

	private int x;
	private int y;
	private int intensity;
	private int radius;
	private int color;

	public PointLight(int x, int y, int intensity, int radius, int color)
	{
		this.x = x;
		this.y = y;
		this.intensity = intensity;
		this.radius = radius;
		this.color = color;
	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}

	public int getIntensity()
	{
		return intensity;
	}

	public int getRadius()
	{
		return radius;
	}

	public int getColor()
	{
		return color;
	}

	public void setX(int x)
	{
		this.x = x;
	}

	public void setY(int y)
	{
		this.y = y;
	}

	public void setIntensity(int intensity)
	{
		this.intensity = intensity;
	}

	public void setRadius(int radius)
	{
		this.radius = radius;
	}

	public void setColor(int color)
	{
		this.color = color;
	}

}
