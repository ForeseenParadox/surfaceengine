package net.vitalrealms.engine.core.rendering.lights;

import java.awt.Color;
import java.awt.Graphics;

import net.vitalrealms.engine.core.Core;
import net.vitalrealms.engine.tile.Tile;
import net.vitalrealms.engine.world.TiledWorld;

public class TiledLightMap
{

	private int width;
	private int height;
	private AmbientLight ambient;

	private int[] lightData;

	public TiledLightMap(TiledWorld world)
	{
		this(world.getWidth(), world.getHeight());
	}

	public TiledLightMap(int width, int height)
	{
		this.width = width;
		this.height = height;
		this.lightData = new int[getWidth() * getHeight()];
		this.ambient = new AmbientLight(255, 0);
	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	public AmbientLight getAmbientLight()
	{
		return ambient;
	}

	public int getLightLevel(int x, int y)
	{
		if (x >= 0 && x < width && y >= 0 && y < height)
		{
			return lightData[x + (y * width)];
		} else
		{
			return 255;
		}
	}

	public void setLightValue(int x, int y, int value)
	{
		if (x >= 0 && x < getWidth() && y >= 0 && y < getHeight())
		{
			lightData[x + (y * width)] = value;
		}
	}

	public void clear(int translateX, int translateY)
	{
		
		int startTileX = -translateX / Tile.getSize();
		int startTileY = -translateY / Tile.getSize();
		int screenTileWidth = Core.getInstance().getWidth() / Tile.getSize() + 1;
		int screenTileHeight = Core.getInstance().getHeight() / Tile.getSize() + 1;

		for (int x = startTileX; x < startTileX + screenTileWidth; x++)
		{
			for (int y = startTileY; y < startTileY + screenTileHeight; y++)
			{
				try
				{
					lightData[x + (y * width)] = ambient.getIntensity();
				} catch (Exception e)
				{
					// error
				}
			}
		}
	}

	public void calcPointLight(PointLight l)
	{

		for (int x = l.getX() - l.getRadius(); x < l.getX() + l.getRadius(); x++)
		{
			for (int y = l.getY() - l.getRadius(); y < l.getY() + l.getRadius(); y++)
			{

				float dist = ((x - l.getX()) * (x - l.getX())) + ((y - l.getY()) * (y - l.getY()));

				if (dist <= (l.getRadius() * l.getRadius()))
				{

					// estimate attenuation

					int light = (int) ((255 * Math.sqrt(dist)) / l.getRadius());
					light += ((255 - light) * (255 - l.getIntensity())) / 255;
					int lightV = (getLightLevel(x, y) * light) / 255;
					setLightValue(x, y, lightV);
				}
			}
		}
	}

	public void render(int translateX, int translateY)
	{

		int startTileX = -translateX / Tile.getSize();
		int startTileY = -translateY / Tile.getSize();
		int screenTileWidth = Core.getInstance().getWidth() / Tile.getSize() + 1;
		int screenTileHeight = Core.getInstance().getHeight() / Tile.getSize() + 1;
		Graphics g = Core.getInstance().getRenderer().getGraphics();

		for (int x = startTileX; x < startTileX + screenTileWidth; x++)
		{
			for (int y = startTileY; y < startTileY + screenTileHeight; y++)
			{
				g.setColor(new Color(0, 0, 0, getLightLevel(x, y)));
				g.fillRect(x * Tile.getSize() + translateX, y * Tile.getSize() + translateY, Tile.getSize(), Tile.getSize());
			}
		}
	}
}
