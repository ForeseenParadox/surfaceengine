package net.vitalrealms.engine.core.rendering;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.VolatileImage;

import net.vitalrealms.engine.core.Core;
import net.vitalrealms.engine.core.sprite.Sprite;

public class Render
{

	private int width;
	private int height;
	private VolatileImage renderedImage;
	private Graphics2D g;
	private boolean antiAlias;

	public Render()
	{
		this.width = Core.getInstance().getWidth();
		this.height = Core.getInstance().getHeight();
		this.renderedImage = Core.getInstance().getCanvas().createVolatileImage(width, height);
		this.g = renderedImage.createGraphics();
		this.antiAlias = true;
	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	public VolatileImage getRenderedImage()
	{
		return renderedImage;
	}

	public Graphics2D getGraphics()
	{
		return g;
	}

	public boolean isAntiAliasEnabled()
	{
		return antiAlias;
	}

	public void setAntiAliasEnabled(boolean antiAlias)
	{
		this.antiAlias = antiAlias;
	}

	public void clear(Color clrColor)
	{
		g.setColor(clrColor);
		g.fillRect(0, 0, width, height);
	}

	public void resize(Canvas canvas)
	{
		this.width = canvas.getWidth();
		this.height = canvas.getHeight();
		this.renderedImage = Core.getInstance().getCanvas().createVolatileImage(width, height);
		this.g = renderedImage.createGraphics();
	}

	public void renderSprite(Sprite sprite, int x, int y)
	{
		this.renderSprite(sprite, x, y, 0);
	}

	public void renderSprite(Sprite sprite, int x, int y, double rot)
	{
		if (sprite != null)
		{
			if (isRectVisible(x, y, sprite.getWidth(), sprite.getHeight()))
			{

				g.translate(x + sprite.getWidth() / 2, y + sprite.getHeight() / 2);
				g.rotate(rot);

				g.drawImage(sprite.getRenderedImage(), -sprite.getWidth() / 2, -sprite.getHeight() / 2, null);

				g.rotate(-rot);
				g.translate(-x - sprite.getWidth() / 2, -y - sprite.getHeight() / 2);
			}
		}
	}

	public boolean isRectVisible(int x, int y, int width, int height)
	{
		if (x + width >= 0 && x < this.width && y + height >= 0 && y < this.height)
		{
			return true;
		} else
		{
			return false;
		}
	}

	public boolean isPointVisible(int x, int y)
	{
		if (x >= 0 && x < width && y > 0 && y < height)
		{
			return true;
		} else
		{
			return false;
		}
	}

	public void update()
	{
		g = (Graphics2D) renderedImage.getGraphics();

		if (antiAlias)
		{
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		}

		// other rendering hints implement later
	}

	public void render(Graphics g)
	{
		g.drawImage(renderedImage, 0, 0, null);
	}

	public void dispose()
	{
		this.renderedImage = null;
		g.dispose();
		this.g = null;
	}
}
