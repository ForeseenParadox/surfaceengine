package net.vitalrealms.engine.core;

import java.awt.Color;

import net.vitalrealms.engine.core.sprite.Sprite;

/**
 * Represents a slash screen. A splash screen is a screen that will display for
 * a brief period of time before the application starts executing and then fades
 * out.
 */
public class SplashScreen
{

	/**
	 * The sprite that will be rendered to the screen.
	 */
	private Sprite image;

	/**
	 * The length of time is milliseconds that this splash screen will display
	 * on the screen.
	 */
	private long length;

	/**
	 * The length of time in milliseconds that is will take to start fading out
	 * the splash screen.
	 */
	private long fade;

	/**
	 * Constructs a splash screen will the specified image, length and fade.
	 */
	public SplashScreen(Sprite image, long length, long fade)
	{
		this.image = image;
		this.length = length;
		this.fade = fade;
	}

	/**
	 * Gets the image that will be rendered to the screen.
	 */
	public Sprite getImage()
	{
		return image;
	}

	/**
	 * Gets the length that the splash screen will display in milliseconds.
	 */
	public long getLength()
	{
		return length;
	}

	/**
	 * Gets the amount of time in milliseconds before the splash screen will
	 * start to fade away.
	 */
	public long getFadeTime()
	{
		return fade;
	}

	/**
	 * Sets the image that will be rendered to the screen.
	 */
	public void setImage(Sprite image)
	{
		this.image = image;
	}

	/**
	 * Sets the length in millseconds that the splash screen will be displayed.
	 */
	public void setLength(long length)
	{
		this.length = length;
	}

	/**
	 * Sets the amount of time in milliseconds that it will take for the splash
	 * screen to start fading.
	 */
	public void setFadeTime(long fade)
	{
		this.fade = fade;
	}

	/**
	 * Renders the splash screen with the specified fade amount.
	 */
	public void render(int fade)
	{

		Core.getInstance().getRenderer().getGraphics().setColor(new Color(0, 0, 0, fade));

		// render
		Core.getInstance()
				.getRenderer()
				.renderSprite(image, (Core.getInstance().getWidth() / 2) - (image.getWidth() / 2),
						(Core.getInstance().getHeight() / 2) - (image.getHeight() / 2));

		Core.getInstance().getRenderer().getGraphics().fillRect(0, 0, Core.getInstance().getWidth(), Core.getInstance().getHeight());
	}
}
