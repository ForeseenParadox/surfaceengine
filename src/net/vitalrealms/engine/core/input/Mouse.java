package net.vitalrealms.engine.core.input;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import net.vitalrealms.engine.core.Core;

public class Mouse extends MouseAdapter
{

	public static final int NUM_BUTTONS = 0xffff;

	public static final int LEFT_BUTTON = 1;
	public static final int MIDDLE_BUTTON = 2;
	public static final int RIGHT_BUTTON = 3;

	private Core core;
	private int x;
	private int y;
	private double scrollMag;
	private boolean[] buttonsDown;
	private boolean visible;

	private long lastUpdate;

	public Mouse(Core core)
	{
		this.core = core;
		this.buttonsDown = new boolean[NUM_BUTTONS];
		core.getCanvas().addMouseListener(this);
		core.getCanvas().addMouseMotionListener(this);
		core.getCanvas().addMouseWheelListener(this);
	}

	public Core getCore()
	{
		return core;
	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}

	public double getScrollMag()
	{
		return scrollMag;
	}

	public boolean[] getButtonsDown()
	{
		return buttonsDown;
	}

	public boolean isButtonDown(int buttonCode)
	{
		return buttonsDown[buttonCode];
	}

	public boolean isVisible()
	{
		return visible;
	}

	public void update()
	{
		if (System.currentTimeMillis() - lastUpdate >= 250)
		{
			lastUpdate = System.currentTimeMillis();
			scrollMag = 0;
		}
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e)
	{
		scrollMag = e.getScrollAmount();
	}

	@Override
	public void mouseDragged(MouseEvent e)
	{
		x = e.getX();
		y = e.getY();
	}

	@Override
	public void mouseMoved(MouseEvent e)
	{
		x = e.getX();
		y = e.getY();
	}

	@Override
	public void mousePressed(MouseEvent e)
	{
		buttonsDown[e.getButton()] = true;
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
		buttonsDown[e.getButton()] = false;
	}

	@Override
	public void mouseEntered(MouseEvent e)
	{
		visible = true;
	}

	@Override
	public void mouseExited(MouseEvent e)
	{
		visible = false;
	}

}
