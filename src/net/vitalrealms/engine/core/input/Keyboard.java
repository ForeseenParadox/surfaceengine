package net.vitalrealms.engine.core.input;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import net.vitalrealms.engine.core.Core;

public class Keyboard extends KeyAdapter
{

	public static final int NUM_KEYS = 0xff;

	private List<Integer> keysPressed = new ArrayList<Integer>();

	private List<Integer> lastFrame = new ArrayList<Integer>();
	private List<Integer> keysDown = new ArrayList<Integer>();

	public Keyboard()
	{
		Core.getInstance().getCanvas().addKeyListener(this);
	}

	public List<Integer> getKeysPressed()
	{
		return keysPressed;
	}

	public List<Integer> getKeysDown()
	{
		return keysDown;
	}

	public boolean isKeyPressed(int keyCode)
	{
		return keysPressed.contains(keyCode);
	}

	public boolean wasKeyPressed(int keyCode)
	{
		return keysDown.contains(keyCode);
	}

	public void update()
	{

		keysDown.clear();
		for (int i = 0; i < NUM_KEYS; i++)
		{
			if (isKeyPressed(i) && !lastFrame.contains((Integer) i))
			{
				keysDown.add(i);
			}
		}

		lastFrame.clear();
		for (int i = 0; i < NUM_KEYS; i++)
		{
			if (isKeyPressed(i))
			{
				lastFrame.add(i);
			}
		}

	}

	@Override
	public void keyPressed(KeyEvent e)
	{
		if (!keysPressed.contains(e.getKeyCode()))
		{
			keysPressed.add(e.getKeyCode());
		}
	}

	@Override
	public void keyReleased(KeyEvent e)
	{
		if (keysPressed.contains(e.getKeyCode()))
		{
			keysPressed.remove((Integer) e.getKeyCode());
		}
	}

}
