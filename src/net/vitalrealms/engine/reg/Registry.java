package net.vitalrealms.engine.reg;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class Registry
{

	private List<TileReg> regTiles;
	private List<EntityReg> regEntities;

	public Registry()
	{
		this.regTiles = new ArrayList<TileReg>();
		this.regEntities = new ArrayList<EntityReg>();
	}

	public List<TileReg> getRegisteredTiles()
	{
		return regTiles;
	}

	public List<EntityReg> getRegisteredEntities()
	{
		return regEntities;
	}

	public void registerTile(TileReg reg)
	{
		this.regTiles.add(reg);
	}

	public void registerEntity(EntityReg reg)
	{
		this.regEntities.add(reg);
	}

	public void deRegisterTile(TileReg reg)
	{
		this.regTiles.remove(reg);
	}

	public void deRegisterEntity(EntityReg reg)
	{
		this.regEntities.remove(reg);
	}

	public TileReg getTRFromClassName(String className)
	{
		for (TileReg reg : regTiles)
		{
			if (reg.getClassName().equals(className))
			{
				return reg;
			}
		}
		return null;
	}

	public TileReg getTRFromID(int id)
	{
		for (TileReg reg : regTiles)
		{
			if (reg.getId() == id)
			{
				return reg;
			}
		}
		return null;
	}

	public TileReg getTRFromDataColor(Color dataColor)
	{
		for (TileReg reg : regTiles)
		{
			if (reg.getDatColor().equals(dataColor))
			{
				return reg;
			}
		}
		return null;
	}

	public EntityReg getERFromClassName(String className)
	{
		for (EntityReg reg : regEntities)
		{
			if (reg.getClassName().equals(className))
			{
				return reg;
			}
		}
		return null;
	}

	public EntityReg getERFromID(int id)
	{
		for (EntityReg reg : regEntities)
		{
			if (reg.getId() == id)
			{
				return reg;
			}
		}
		return null;
	}

}
