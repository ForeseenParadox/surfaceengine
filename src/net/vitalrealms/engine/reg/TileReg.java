package net.vitalrealms.engine.reg;

import java.awt.Color;

import net.vitalrealms.engine.core.sprite.Sprite;
import net.vitalrealms.engine.tile.Tile;
import net.vitalrealms.engine.tile.TileLocation;

public class TileReg
{

	private String className;
	private int id;
	private Color datColor;

	public TileReg(String className, int id, Color datColor)
	{
		this.className = className;
		this.id = id;
		this.datColor = datColor;
	}

	public String getClassName()
	{
		return className;
	}

	public void setClassName(String className)
	{
		this.className = className;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public Color getDatColor()
	{
		return datColor;
	}

	public void setDatColor(Color datColor)
	{
		this.datColor = datColor;
	}

	public Tile construct(TileLocation loc, Sprite sprite, boolean solid) throws Exception
	{
		return (Tile) Class.forName(className).getConstructor(TileLocation.class, Sprite.class, Boolean.class)
				.newInstance(loc, sprite, solid);
	}

	public Tile construct(TileLocation loc, boolean solid) throws Exception
	{
		return (Tile) Class.forName(className).getConstructor(TileLocation.class, Boolean.class)
				.newInstance(loc, solid);
	}

	public Tile construct(TileLocation loc) throws Exception
	{
		return (Tile) Class.forName(className).getConstructor(TileLocation.class).newInstance(loc);
	}
}
