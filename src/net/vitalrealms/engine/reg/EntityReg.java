package net.vitalrealms.engine.reg;

public class EntityReg
{

	private String className;
	private int id;

	public EntityReg(String className, int id)
	{
		this.className = className;
		this.id = id;
	}

	public String getClassName()
	{
		return className;
	}

	public void setClassName(String className)
	{
		this.className = className;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

}
