package net.vitalrealms.engine.tile;

import net.vitalrealms.engine.core.rendering.Render;
import net.vitalrealms.engine.core.sprite.Sprite;

public abstract class Tile
{

	private static int size = 16;

	private TileLocation loc;
	private Sprite sprite;
	private boolean solid;

	public Tile(TileLocation loc, Sprite sprite, boolean solid)
	{
		this.loc = loc;
		this.sprite = sprite;
		this.solid = solid;
	}

	public static int getSize()
	{
		return size;
	}

	public TileLocation getLoc()
	{
		return loc;
	}

	public void setLoc(TileLocation loc)
	{
		this.loc = loc;
	}

	public Sprite getSprite()
	{
		return sprite;
	}

	public boolean isSolid()
	{
		return solid;
	}

	public static void setSize(int size)
	{
		Tile.size = size;
	}

	public void setSprite(Sprite sprite)
	{
		this.sprite = sprite;
	}

	public void setSolid(boolean solid)
	{
		this.solid = solid;
	}

	public void render(Render render, int translateX, int translateY)
	{
		render.renderSprite(sprite, size * loc.getX() + translateX, size * loc.getY() + translateY);
	}

}
