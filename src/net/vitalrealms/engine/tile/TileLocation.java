package net.vitalrealms.engine.tile;

import net.vitalrealms.engine.utils.math.Vector2f;

public class TileLocation
{

	private int x;
	private int y;

	public TileLocation()
	{
		this.x = 0;
		this.y = 0;
	}

	public TileLocation(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}

	public void setX(int x)
	{
		this.x = x;
	}

	public void setY(int y)
	{
		this.y = y;
	}

	public Vector2f toVector2f()
	{
		return new Vector2f(x, y);
	}

}
