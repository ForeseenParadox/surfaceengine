package net.vitalrealms.engine.world;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import javax.imageio.ImageIO;

import net.vitalrealms.engine.reg.Registry;
import net.vitalrealms.engine.tile.Tile;
import net.vitalrealms.engine.tile.TileLocation;

public class TiledWorldLoader
{

	private Registry reg;

	public TiledWorldLoader()
	{
		this.reg = new Registry();
	}

	public Registry getRegistry()
	{
		return reg;
	}

	public TiledWorld loadWorld(String file) throws Exception
	{
		return loadWorld(new File(file));
	}

	public TiledWorld loadWorld(File file) throws Exception
	{
		if (!file.isDirectory())
		{
			throw new IllegalArgumentException("When loading a world, a valid directory must be specified");
		} else
		{

			String name = "";
			int width = 0;
			int height = 0;
			BufferedImage tileImage;
			TiledWorld world;

			// load props
			Properties props = new Properties();
			props.load(new FileInputStream(file + "\\worldMeta.properties"));

			name = props.getProperty("name");
			width = Integer.parseInt((String) props.get("width"));
			height = Integer.parseInt((String) props.get("height"));
			
			System.out.println(name);

			// instantiate world
			world = new TiledWorld(name, width, height);

			// load tiles
			tileImage = ImageIO.read(new File(file.getAbsolutePath() + "\\tileData.png"));
			int[] pixelData = tileImage.getRGB(0, 0, width, height, null, 0, width);

			for (int x = 0; x < width; x++)
			{
				for (int y = 0; y < height; y++)
				{
					Color dataColor = new Color(pixelData[x + (y * width)]);
					if (reg.getTRFromDataColor(dataColor) != null)
					{
						Tile t = reg.getTRFromDataColor(dataColor).construct(new TileLocation(x, y));
						if (t != null)
						{
							world.addTile(t);
						}
					}
				}
			}

			return world;

		}
	}
}
