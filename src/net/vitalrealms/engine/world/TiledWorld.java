package net.vitalrealms.engine.world;

import java.util.ArrayList;
import java.util.List;

import net.vitalrealms.engine.core.Core;
import net.vitalrealms.engine.core.rendering.lights.TiledLightMap;
import net.vitalrealms.engine.entity.Entity;
import net.vitalrealms.engine.entity.immobileEntity.ImmobileEntity;
import net.vitalrealms.engine.entity.mob.Mob;
import net.vitalrealms.engine.entity.particle.Particle;
import net.vitalrealms.engine.entity.projectile.Projectile;
import net.vitalrealms.engine.tile.Tile;
import net.vitalrealms.engine.tile.TileLocation;
import net.vitalrealms.engine.utils.math.Vector2f;

public class TiledWorld
{

	public final Vector2f GRAVITY_VECTOR;

	private String name;
	private int width;
	private int height;
	private Tile[][] tiles;
	private List<Mob> mobs;
	private List<Projectile> projectiles;
	private List<Particle> particles;
	private List<ImmobileEntity> immobileEntities;
	private TiledLightMap lightMap;

	private int translateX;
	private int translateY;

	public TiledWorld(String name, int width, int height)
	{
		this(name, width, height, new Vector2f(0, 0.1f));
	}

	public TiledWorld(String name, int width, int height, Vector2f gravity)
	{
		this.name = name;
		this.width = width;
		this.height = height;
		this.tiles = new Tile[width][height];
		this.mobs = new ArrayList<Mob>();
		this.projectiles = new ArrayList<Projectile>();
		this.particles = new ArrayList<Particle>();
		this.immobileEntities = new ArrayList<ImmobileEntity>();
		this.lightMap = new TiledLightMap(this);
		GRAVITY_VECTOR = gravity;
	}

	public String getName()
	{
		return name;
	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	public Tile[][] getTiles()
	{
		return tiles;
	}

	public Tile getTileAt(TileLocation loc)
	{
		return tiles[loc.getX()][loc.getY()];
	}

	public Tile getTileAt(float x, float y)
	{
		try
		{
			return tiles[(int) (x / Tile.getSize())][(int) (y / Tile.getSize())];
		} catch (ArrayIndexOutOfBoundsException e)
		{
			return null;
		}
	}

	public List<Mob> getMobs()
	{
		return mobs;
	}

	public List<Projectile> getProjectiles()
	{
		return projectiles;
	}

	public List<Particle> getParticles()
	{
		return particles;
	}

	public List<ImmobileEntity> getImmobileEntities()
	{
		return immobileEntities;
	}

	public TiledLightMap getLightMap()
	{
		return lightMap;
	}

	public int getTranslateX()
	{
		return translateX;
	}

	public int getTranslateY()
	{
		return translateY;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void addTile(Tile tile)
	{
		if (tile.getLoc().getX() >= 0 && tile.getLoc().getY() >= 0)
		{
			tiles[tile.getLoc().getX()][tile.getLoc().getY()] = tile;
		} else
		{
			throw new IllegalArgumentException("Tile out of bounds!");
		}
	}

	public void removeTile(int x, int y)
	{
		if (x >= 0 && x < width)
		{
			if (y >= 0 && y < height)
			{
				tiles[x][y] = null;
			}
		}
	}

	public void addMob(Mob mob)
	{
		if (mob != null)
		{
			mobs.add(mob);
		}
	}

	public void addProjectile(Projectile projectile)
	{
		if (projectile != null)
		{
			projectiles.add(projectile);
		}
	}

	public void addParticle(Particle particle)
	{
		if (particle != null)
		{
			particles.add(particle);
		}
	}

	public void addImmobileEntity(ImmobileEntity immobileEntity)
	{
		if (immobileEntity != null)
		{
			this.immobileEntities.add(immobileEntity);
		}
	}

	public void removeMob(Mob mob)
	{
		if (mob != null)
		{
			mobs.remove(mob);
		}
	}

	public void removeProjectile(Projectile projectile)
	{
		if (projectile != null)
		{
			projectiles.remove(projectile);
		}
	}

	public void removeParticle(Particle particle)
	{
		if (particle != null)
		{
			particles.remove(particle);
		}
	}

	public void removeImmobileEntity(ImmobileEntity immobileEntity)
	{
		if (immobileEntity != null)
		{
			immobileEntities.remove(immobileEntity);
		}
	}

	public void setTranslateX(int translateX)
	{
		this.translateX = translateX;
	}

	public void setTranslateY(int translateY)
	{
		this.translateY = translateY;
	}

	public void translateX(int mag)
	{
		this.translateX += mag;
	}

	public void translateY(int mag)
	{
		this.translateY += mag;
	}

	public void centerCameraOnEntity(Entity e)
	{
		this.translateX = (int) -e.getX() + (Core.getInstance().getWidth() / 2 - e.getWidth() / 2);
		this.translateY = (int) -e.getY() + (Core.getInstance().getHeight() / 2 - e.getHeight() / 2);
	}

	public void update()
	{
		for (int i = 0; i < mobs.size(); i++)
		{
			Mob m = mobs.get(i);
			if (m.isRemoved())
			{
				removeMob(m);
			} else
			{
				m.update();
			}
		}

		for (int i = 0; i < projectiles.size(); i++)
		{
			Projectile p = projectiles.get(i);
			if (p.isRemoved())
			{
				removeProjectile(p);
			} else
			{
				p.update();
			}
		}

		for (int i = 0; i < particles.size(); i++)
		{
			Particle p = particles.get(i);
			if (p.isRemoved())
			{
				removeParticle(p);
			} else
			{
				p.update();
			}
		}

		for (int i = 0; i < immobileEntities.size(); i++)
		{
			ImmobileEntity ie = immobileEntities.get(i);
			if (ie.isRemoved())
			{
				removeImmobileEntity(ie);
			} else
			{
				ie.update();
			}
		}
	}

	public void render()
	{
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				if (tiles[x][y] != null)
				{
					tiles[x][y].render(Core.getInstance().getRenderer(), translateX, translateY);
				}
			}
		}

		for (Mob m : mobs)
		{
			m.render(Core.getInstance().getRenderer(), translateX, translateY);
		}

		for (Projectile p : projectiles)
		{
			p.render(Core.getInstance().getRenderer(), translateX, translateY);
		}

		for (Particle p : particles)
		{
			p.render(Core.getInstance().getRenderer(), translateX, translateY);
		}

		for (ImmobileEntity ie : immobileEntities)
		{
			ie.render(Core.getInstance().getRenderer(), translateX, translateY);
		}

	}

}
