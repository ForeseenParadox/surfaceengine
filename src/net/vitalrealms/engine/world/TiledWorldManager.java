package net.vitalrealms.engine.world;

import java.util.ArrayList;
import java.util.List;

public class TiledWorldManager
{

	private TiledWorldLoader loader;
	private List<TiledWorld> loadedWorlds;
	private TiledWorld currentWorld;

	public TiledWorldManager()
	{
		loader = new TiledWorldLoader();
		loadedWorlds = new ArrayList<TiledWorld>();
	}

	public TiledWorldLoader getWorldLoader()
	{
		return loader;
	}

	public List<TiledWorld> getLoadedWorlds()
	{
		return loadedWorlds;
	}

	public TiledWorld getCurrentWorld()
	{
		return currentWorld;
	}

	public TiledWorld getWorldByName(String worldName)
	{
		for (TiledWorld world : loadedWorlds)
		{
			if (world.getName().equalsIgnoreCase(worldName))
			{
				return world;
			}
		}
		return null;
	}

	public void setCurrentWorld(TiledWorld world)
	{
		this.currentWorld = world;
	}

	public void setCurrentWorld(String worldName)
	{
		this.currentWorld = getWorldByName(worldName);
	}

	public void loadWorld(String dir)
	{
		try
		{
			TiledWorld world = loader.loadWorld(dir);
			loadedWorlds.add(world);
		} catch (Exception e)
		{
			System.err.println("Error loading world: ");
			e.printStackTrace();
		}

	}

	public void loadWorld(TiledWorld world)
	{
		if (world != null)
		{
			this.loadedWorlds.add(world);
		}
	}
	
}
