package net.vitalrealms.engine.entity.projectile;

import net.vitalrealms.engine.core.sprite.Sprite;
import net.vitalrealms.engine.entity.Entity;
import net.vitalrealms.engine.utils.math.Vector2f;
import net.vitalrealms.engine.world.TiledWorld;

public abstract class Projectile extends Entity {

	protected Vector2f vel;

	public Projectile(TiledWorld world, float x, float y, double rot,
			Sprite sprite) {
		super(world, x, y, rot, sprite);
	}

	public Vector2f getVelocity() {
		return vel;
	}

	public void setVelocity(Vector2f vel) {
		this.vel = vel;
	}

	public abstract void update();

}
