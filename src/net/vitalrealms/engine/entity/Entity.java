package net.vitalrealms.engine.entity;

import net.vitalrealms.engine.core.rendering.Render;
import net.vitalrealms.engine.core.sprite.Sprite;
import net.vitalrealms.engine.tile.Tile;
import net.vitalrealms.engine.tile.TileLocation;
import net.vitalrealms.engine.utils.math.Vector2f;
import net.vitalrealms.engine.world.TiledWorld;

public abstract class Entity
{

	private static boolean gravityEnabled = true;

	protected TiledWorld world;
	protected float x;
	protected float y;
	protected double rot;
	protected int width;
	protected int height;
	protected Sprite currentSprite;
	protected boolean tileClip;
	protected boolean antiGravity;
	protected boolean removed;

	protected Vector2f accel;
	protected Vector2f vel;
	protected Vector2f maxAccel;
	protected Vector2f maxVel;

	public Entity(TiledWorld world, TileLocation loc, Sprite sprite)
	{
		this(world, loc, 0, sprite);
	}

	public Entity(TiledWorld world, TileLocation loc, float rot, Sprite sprite)
	{
		this(world, loc.getX() * Tile.getSize(), loc.getY() * Tile.getSize(), rot, sprite);
	}

	public Entity(TiledWorld world, float x, float y, Sprite sprite)
	{
		this(world, x, y, 0, sprite);
	}

	public Entity(TiledWorld world, float x, float y, double rot, Sprite sprite)
	{
		this.world = world;
		this.x = x;
		this.y = y;
		this.rot = rot;
		this.currentSprite = sprite;

		if (sprite == null)
		{
			this.width = 0;
			this.height = 0;
		} else
		{
			this.width = sprite.getWidth();
			this.height = sprite.getHeight();
		}

		removed = false;

		// implement later ctor
		tileClip = false;
		antiGravity = false;

		this.accel = new Vector2f();
		this.vel = new Vector2f();
		this.maxVel = new Vector2f(3, 3);
		this.maxAccel = new Vector2f(1, 1);
	}

	public static boolean isGravityEnabled()
	{
		return gravityEnabled;
	}

	public static void setGravityEnabled(boolean gravityEnabled)
	{
		Entity.gravityEnabled = gravityEnabled;
	}

	public TiledWorld getWorld()
	{
		return world;
	}

	public float getX()
	{
		return x;
	}

	public float getY()
	{
		return y;
	}

	public TileLocation getLocation()
	{
		return new TileLocation((int) (x / Tile.getSize()), (int) (y / Tile.getSize()));
	}

	public double getRot()
	{
		return rot;
	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	public Sprite getCurrentSprite()
	{
		return currentSprite;
	}

	public boolean canTileClip()
	{
		return tileClip;
	}

	public boolean hasAntiGravity()
	{
		return antiGravity;
	}

	public float midpointX()
	{
		return x + width / 2;
	}

	public float midpointY()
	{
		return y + height / 2;
	}

	public float deltaX(Entity e)
	{
		return midpointX() - e.midpointX();
	}

	public float deltaY(Entity e)
	{
		return midpointY() - e.midpointY();
	}

	public double dist(Entity e)
	{
		double deltaX = deltaX(e);
		double deltaY = deltaY(e);
		return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
	}

	public boolean hasCollidedWithEntity(Entity e)
	{
		if ((x + width >= e.getX()) && (x <= e.getX() + e.getWidth()))
		{
			if ((y + height >= e.getY()) && (y <= e.getY() + e.getHeight()))
			{
				return true;
			}
		}
		return false;
	}

	public boolean tileCollisionX(float dx)
	{

		int vert = (width / Tile.getSize()) + 1;
		float[] tarX = new float[vert];
		float[] tarY = new float[vert];

		for (int i = 0; i < vert; i++)
		{
			if (dx > 0)
			{
				tarX[i] = x + (width - 1) + dx;
			} else
			{
				tarX[i] = x + dx;
			}
			tarY[i] = y + (i * (height - 1));
		}

		for (int i = 0; i < vert; i++)
		{
			float posX = tarX[i];
			float posY = tarY[i];
			Tile t = world.getTileAt(posX, posY);

			if (t != null)
			{
				if (t.isSolid())
				{
					return true;
				}
			}
		}

		return false;
	}

	public boolean tileCollisionY(float dy)
	{

		int vert = (height / Tile.getSize()) + 1;
		float[] tarX = new float[vert];
		float[] tarY = new float[vert];

		for (int i = 0; i < vert; i++)
		{
			if (dy > 0)
			{
				tarY[i] = y + (height - 1) + dy;
			} else
			{
				tarY[i] = y + dy;
			}
			tarX[i] = x + (i * (width - 1));
		}

		for (int i = 0; i < vert; i++)
		{
			float posX = tarX[i];
			float posY = tarY[i];
			Tile t = world.getTileAt(posX, posY);

			if (t != null)
			{
				if (t.isSolid())
				{
					return true;
				}
			}
		}

		return false;
	}

	public boolean tileCollision(Vector2f vec)
	{
		if (tileCollisionX(vec.getX()) || tileCollisionY(vec.getY()))
		{
			return true;
		} else
		{
			return false;
		}
	}

	public boolean tileAbove()
	{
		return tileCollisionY(-1);
	}

	public boolean tileBelow()
	{
		return tileCollisionY(1);
	}

	public boolean tileRight()
	{
		return tileCollisionX(1);
	}

	public boolean tileLeft()
	{
		return tileCollisionX(-1);
	}

	public Tile tileDisplacedX(float dx)
	{
		if (dx > 0)
		{
			return world.getTileAt(x + width + dx, y);
		} else if (dx < 0)
		{
			return world.getTileAt(x + dx, y);
		} else
		{
			return world.getTileAt(x, y);
		}

	}

	public Tile tileDisplacedY(float dy)
	{
		if (dy > 0)
		{
			return world.getTileAt(x, y + height + dy);
		} else if (dy < 0)
		{
			return world.getTileAt(x, y + dy);
		} else
		{
			return world.getTileAt(x, y);
		}

	}

	public Tile tileDisplaced(float dx, float dy)
	{
		float x = 0;
		float y = 0;

		if (dx > 0)
		{
			x = this.x + width + dx;
		} else if (dx < 0)
		{
			x = this.x - 1;
		}

		if (dy > 0)
		{
			y = this.y + height + dy;
		} else if (dy < 0)
		{
			y = this.y - 1;
		}

		return world.getTileAt(x, y);

	}

	public boolean isRemoved()
	{
		return removed;
	}

	public Vector2f getAcceleration()
	{
		return accel;
	}

	public Vector2f getVelocity()
	{
		return vel;
	}

	public Vector2f getMaxAcceleration()
	{
		return maxAccel;
	}

	public Vector2f getMaxVelocity()
	{
		return maxVel;
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public void setY(float y)
	{
		this.y = y;
	}

	public void tendAccelerationXToZero(float dx)
	{
		tendAccelerationXTo(0, dx);
	}

	public void tendAccelerationXTo(float value, float dx)
	{
		if (this.accel.getX() != value)
		{

			dx = Math.abs(dx);

			if (this.accel.getX() > value)
			{
				this.accel.setX(this.accel.getX() - dx);

				if (this.accel.getX() < value)
				{
					this.accel.setX(value);
				}
			} else if (this.accel.getX() < value)
			{
				this.accel.setX(this.accel.getX() + dx);

				if (this.accel.getX() > value)
				{
					this.accel.setX(value);
				}
			}

		}
	}

	public void tendAccelerationYToZero(float dy)
	{
		tendAccelerationYTo(0, dy);
	}

	public void tendAccelerationYTo(float value, float dy)
	{
		if (this.accel.getY() != value)
		{

			dy = Math.abs(dy);

			if (this.accel.getY() > value)
			{
				this.accel.setY(this.accel.getY() - dy);

				if (this.accel.getY() < value)
				{
					this.accel.setY(value);
				}
			} else if (this.accel.getY() < value)
			{
				this.accel.setY(this.accel.getY() + dy);

				if (this.accel.getY() > value)
				{
					this.accel.setY(value);
				}
			}

		}
	}

	public void tendVelocityXToZero(float dx)
	{
		tendVelocityXTo(0, dx);
	}

	public void tendVelocityXTo(float value, float dx)
	{
		if (this.accel.getX() == 0)
		{
			if (this.vel.getX() != value)
			{

				dx = Math.abs(dx);

				if (this.vel.getX() > value)
				{
					this.vel.setX(this.vel.getX() - dx);

					if (this.vel.getX() < value)
					{
						this.vel.setX(0);
					}
				} else if (this.vel.getX() < value)
				{
					this.vel.setX(this.vel.getX() + dx);

					if (this.vel.getX() > value)
					{
						this.vel.setX(value);
					}
				}

			}
		}
	}

	public void tendVelocityYToZero(float dy)
	{
		tendVelocityYTo(0, dy);
	}

	public void tendVelocityYTo(float value, float dy)
	{
		if (this.accel.getY() == 0)
		{
			if (this.vel.getY() != value)
			{

				dy = Math.abs(dy);

				if (this.vel.getY() > value)
				{
					this.vel.setY(this.vel.getY() - dy);

					if (this.vel.getY() < value)
					{
						this.vel.setY(value);
					}
				} else if (this.vel.getY() < value)
				{
					this.vel.setY(this.vel.getY() + dy);

					if (this.vel.getY() > value)
					{
						this.vel.setY(value);
					}
				}

			}
		}
	}

	public void move(float magX, float magY)
	{
		// handle x
		if (!tileClip)
		{
			if (!tileCollisionX(magX))
			{
				x += magX;
			} else
			{

				/*
				 * implement this better later(collision error checking), it was
				 * a pain during ludum dare
				 */

				if (!tileRight() && !tileLeft())
				{

					for (int i = 0; i < (width / Tile.getSize()) + 1; i++)
					{

						Tile tDx = tileDisplaced(magX, magY + i * Tile.getSize());

						if (tDx != null)
						{
							float x = 0;
							if (magX > 0)
							{
								x = (tDx.getLoc().getX() * Tile.getSize()) - width;
							} else if (magY < 0)
							{
								x = (tDx.getLoc().getX() * Tile.getSize()) + width;
							}

							this.x = x;
							break;
						}
					}
				}

				vel.setX(0);
				accel.setX(0);

			}

			if (!tileCollisionY(magY))
			{
				y += magY;
			} else
			{
				/*
				 * implement this better later(collision error checking), it was
				 * a pain during ludum dare
				 */

				if (!tileBelow() && !tileAbove())
				{
					for (int i = 0; i < (width / Tile.getSize()) + 1; i++)
					{
						Tile tDy = tileDisplaced(magX + i * Tile.getSize(), magY);
						if (tDy != null)
						{
							float y = 0;
							if (magY > 0)
							{
								y = (tDy.getLoc().getY() * Tile.getSize()) - height;
							} else if (magY < 0)
							{
								y = (tDy.getLoc().getY() * Tile.getSize()) + height;
							}

							this.y = y;
							break;
						}
					}
				}

				vel.setY(0);
				accel.setY(0);

			}
		} else
		{
			x += magX;
			y += magY;
		}
	}

	public void setRot(float rot)
	{
		this.rot = rot;
	}

	public void setCurrentSprite(Sprite sprite)
	{
		this.currentSprite = sprite;
	}

	public void setTileClip(boolean tileClip)
	{
		this.tileClip = tileClip;
	}

	public void setAntiGravity(boolean antiGravity)
	{
		this.antiGravity = antiGravity;
	}

	public void remove()
	{
		removed = true;
	}

	public void setAcceleration(Vector2f accel)
	{
		if (accel != null)
		{
			this.accel = accel;
		}
	}

	public void setVelocity(Vector2f vel)
	{
		if (vel != null)
		{
			this.vel = vel;
		}
	}

	public void setMaxAcceleration(Vector2f maxAccel)
	{
		if (maxAccel != null)
		{
			this.maxAccel = maxAccel;
		}
	}

	public void setMaxVelocity(Vector2f maxVel)
	{
		if (maxVel != null)
		{
			this.maxVel = maxVel;
		}
	}

	public void setAcceleration(float dx, float dy)
	{
		this.accel = new Vector2f(dx, dy);
	}

	public void setVelocity(float dx, float dy)
	{
		this.vel = new Vector2f(dx, dy);
	}

	public void setMaxAcceleration(float dx, float dy)
	{
		this.maxAccel = new Vector2f(dx, dy);
	}

	public void setMaxVelocity(float dx, float dy)
	{
		this.maxVel = new Vector2f(dx, dy);
	}

	public void updateMovement()
	{

		// add
		this.vel.set(this.vel.add(accel));
		this.move(vel.getX(), vel.getY());

		// accel

		if (this.accel.getX() > this.maxAccel.getX())
		{
			this.accel.setX(this.maxAccel.getX());
		}

		if (this.accel.getX() < -this.maxAccel.getX())
		{
			this.accel.setX(-this.maxAccel.getX());
		}

		if (this.accel.getY() > this.maxAccel.getY())
		{
			this.accel.setY(this.maxAccel.getY());
		}

		if (this.accel.getY() < -this.maxAccel.getY())
		{
			this.accel.setY(-this.maxAccel.getY());
		}

		// vel

		if (this.vel.getX() > this.maxVel.getX())
		{
			this.vel.setX(this.maxVel.getX());
		}

		if (this.vel.getX() < -this.maxVel.getX())
		{
			this.vel.setX(-this.maxVel.getX());
		}

		if (this.vel.getY() > this.maxVel.getY())
		{
			this.vel.setY(this.maxVel.getY());
		}

		if (this.vel.getY() < -this.maxVel.getY())
		{
			this.vel.setY(-this.maxVel.getY());
		}

		if (!tileBelow())
		{
			if (accel.getY() < 0)
			{
				tendAccelerationYToZero(world.GRAVITY_VECTOR.getY());
			}
			if (accel.getY() == 0)
			{
				accel.setY(world.GRAVITY_VECTOR.getY());
			}
		} else
		{
			if (!tileClip)
			{
				accel.setY(0);
				vel.setY(0);
			}
		}

	}

	public abstract void update();

	public void render(Render render, int translateX, int translateY)
	{
		render.renderSprite(currentSprite, (int) x + translateX, (int) y + translateY, rot);
	}

}
