package net.vitalrealms.engine.entity.immobileEntity;

import net.vitalrealms.engine.core.sprite.Sprite;
import net.vitalrealms.engine.entity.Entity;
import net.vitalrealms.engine.world.TiledWorld;

public abstract class ImmobileEntity extends Entity {

	public ImmobileEntity(TiledWorld world, float x, float y, float rot,
			Sprite sprite) {
		super(world, x, y, rot, sprite);
	}

	public abstract void update();

}
