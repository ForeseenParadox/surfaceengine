package net.vitalrealms.engine.entity.mob;

import net.vitalrealms.engine.core.sprite.Sprite;
import net.vitalrealms.engine.entity.Entity;
import net.vitalrealms.engine.utils.math.Vector2f;
import net.vitalrealms.engine.world.TiledWorld;

public abstract class Mob extends Entity {

	// jumping
	protected boolean jumping;
	protected float jumpVel;

	public Mob(TiledWorld world, float x, float y, double rot, Sprite sprite) {
		super(world, x, y, rot, sprite);

		this.accel = new Vector2f();
		this.vel = new Vector2f();
		this.maxAccel = new Vector2f(1, 3);
		this.maxVel = new Vector2f(1, 3);

		this.jumping = false;
		this.jumpVel = -10.5f;
	}

	public boolean isJumping() {
		return jumping;
	}

	public float getJumpAcceleration() {
		return jumpVel;
	}

	public void jump() {
		if (!tileAbove() && tileBelow()) {
			vel.setY(jumpVel);
			jumping = true;
		}
	}

	public void updateMovement() {

		super.updateMovement();

		// gravity and jumping
		if (jumping) {

			// handle jumping

			if (vel.getY() >= 0) {

				vel.setY(0);
				accel.setY(0);

				jumping = false;

			}
			if (tileAbove()) {

				vel.setY(0);
				accel.setY(world.GRAVITY_VECTOR.getY());
				jumping = false;

			}

		} else {
			/*
			 * // handle gravity if (!tileBelow()) {
			 * accel.setY(world.GRAVITY_VECTOR.getY()); } else { accel.setY(0);
			 * vel.setY(0); }
			 */

		}

	}

	public abstract void update();

}
