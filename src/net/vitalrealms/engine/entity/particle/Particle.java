package net.vitalrealms.engine.entity.particle;

import net.vitalrealms.engine.core.sprite.Sprite;
import net.vitalrealms.engine.entity.Entity;
import net.vitalrealms.engine.utils.Rand;
import net.vitalrealms.engine.utils.math.Vector2f;
import net.vitalrealms.engine.world.TiledWorld;

public abstract class Particle extends Entity
{

	protected int life;
	protected int ticksLived;

	public Particle(TiledWorld world, float x, float y, float speed, int life, Sprite sprite)
	{
		super(world, x, y, 0, sprite);
		this.vel = new Vector2f((float) Rand.getRandomFloat() * speed, (float) Rand.getRandomFloat() * speed);
		this.life = life;
		this.ticksLived = 0;
		super.tileClip = true;
	}

	public Particle(TiledWorld world, float x, float y, double rot, Sprite sprite, float destX, float destY, float speed)
	{

		super(world, x, y, rot, sprite);
		double angle = Math.atan2(destY - y, destX - x);
		vel = new Vector2f((float) Math.cos(angle), (float) Math.sin(angle));
		vel.scale(speed);

		this.life = 50000000;
		this.ticksLived = 0;
	}

	@Override
	public void update()
	{
		super.updateMovement();

		ticksLived++;

		if (ticksLived >= life)
		{
			remove();
		}

	}

}
