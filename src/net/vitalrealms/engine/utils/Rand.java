package net.vitalrealms.engine.utils;

import java.util.Random;

public class Rand
{

	private static final Random RAND = new Random();

	public static boolean getRandomBool()
	{
		return RAND.nextBoolean();
	}

	public static byte getRandomByte()
	{
		byte[] b = new byte[1];
		RAND.nextBytes(b);
		return b[0];
	}

	public static short getRandomShort(short min, short max)
	{
		return (short) (RAND.nextInt(max - min) + min);
	}

	public static char getRandomChar()
	{
		return (char) (getRandomInt(0, Short.MAX_VALUE));
	}

	public static int getRandomInt(int min, int max)
	{
		return RAND.nextInt(max - min) + min;
	}

	public static long getRandomLong()
	{
		return RAND.nextLong();
	}

	public static float getRandomFloat()
	{
		return RAND.nextFloat();
	}

	public static double getRandomDouble()
	{
		return RAND.nextDouble();
	}

	public static double normDist()
	{
		return RAND.nextGaussian();
	}

}
