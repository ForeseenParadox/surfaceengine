package net.vitalrealms.engine.utils.math;

public class Vector2f
{

	private float x;
	private float y;

	public Vector2f()
	{
		this.x = 0;
		this.y = 0;
	}

	public Vector2f(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	public Vector2f(float r)
	{
		this.x = r;
		this.y = r;
	}

	public Vector2f(Vector2f vec)
	{
		this.x = vec.getX();
		this.y = vec.getY();
	}

	public float getX()
	{
		return x;
	}

	public float getY()
	{
		return y;
	}

	public Vector2f add(Vector2f vec)
	{
		return new Vector2f(x + vec.getX(), y + vec.getY());
	}

	public Vector2f sub(Vector2f vec)
	{
		return new Vector2f(x - vec.getX(), y - vec.getY());
	}

	public Vector2f mul(Vector2f vec)
	{
		return new Vector2f(x * vec.getX(), y * vec.getY());
	}

	public double length()
	{
		return Math.sqrt(x * x + y * y);
	}

	public double rotation()
	{
		return Math.atan2(y, x);
	}

	public double dot(Vector2f vec)
	{
		return x * vec.getX() + y * vec.getY();
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public void setY(float y)
	{
		this.y = y;
	}

	public void setZero()
	{
		this.x = 0;
		this.y = 0;
	}

	public void set(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	public void set(float r)
	{
		this.x = r;
		this.y = r;
	}

	public void set(Vector2f vec)
	{
		this.x = vec.getX();
		this.y = vec.getY();
	}

	public void invert()
	{
		this.x *= (-1);
		this.y *= (-1);
	}

	public void scale(float scaler)
	{
		this.x *= scaler;
		this.y *= scaler;
	}

	public void normalize()
	{
		float length = (float) length();
		x /= length;
		y /= length;
	}

	public void rotate(float theta)
	{
		float newX = (float) (x * Math.cos(theta) + y * Math.sin(theta));
		float newY = (float) (x * Math.sin(theta) - y * Math.cos(theta));
		x = newX;
		y = newY;
	}
}
