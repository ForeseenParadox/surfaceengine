package net.vitalrealms.engine.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class LineCounter
{

	public static final String PATH = LineCounter.class.getProtectionDomain().getCodeSource().getLocation().getFile()
			.replace("/bin", "")
			+ "src/net/vitalrealms/";

	public static void main(String[] args)
	{

		System.out.println(PATH);

		List<File> allFiles = listAllFiles(new File(PATH));
		int lines = 0;

		for (File f : allFiles)
		{
			System.out.println(f.getName());
			lines += lineCount(f);
		}

		System.out.println(lines + " lines of code in " + allFiles.size() + " files, avg = "
				+ ((double) lines / allFiles.size()));
	}

	public static int lineCount(File file)
	{
		BufferedReader stream = null;
		try
		{
			stream = new BufferedReader(new FileReader(file));
			int count = 0;
			while (stream.readLine() != null)
			{
				count++;
			}
			stream.close();
			return count;
		} catch (Exception e)
		{
			return 0;
		}
	}

	public static List<File> listAllFiles(File parent)
	{
		List<File> res = new ArrayList<File>();
		for (File f : parent.listFiles())
		{
			if (f.isDirectory())
			{
				res.addAll(listAllFiles(f));
			} else
			{
				res.add(f);
			}
		}
		return res;
	}
}
