package net.vitalrealms.engine.utils;

public class Timer
{

	private long elapsed;
	private long last;

	public Timer()
	{
		reset();
	}

	public long getElapsed()
	{
		return elapsed;
	}

	public void reset()
	{
		this.elapsed = 0;
		this.last = System.currentTimeMillis();
	}

	public void update()
	{
		this.elapsed += (System.currentTimeMillis() - last);// delta
		this.last = System.currentTimeMillis();
	}

}
