package net.vitalrealms.engine.utils;

import java.awt.Graphics;

import net.vitalrealms.engine.core.Core;

public class StringUtils
{

	public static int getWidthOfString(String text, Graphics g)
	{
		return (int) (Core.getInstance().getRenderer().getGraphics().getFontMetrics().getStringBounds(text, g))
				.getWidth();
	}

	public static int getHeightOfString(String text, Graphics g)
	{
		return (int) (Core.getInstance().getRenderer().getGraphics().getFontMetrics().getStringBounds(text, g))
				.getHeight();
	}

}
