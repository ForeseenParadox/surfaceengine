package net.vitalrealms.engine.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import net.vitalrealms.engine.core.Core;

public class IOUtils
{

	public static String getProgramPath()
	{
		try
		{
			return URLDecoder.decode(
					(new File(Core.class.getProtectionDomain().getCodeSource().getLocation().getPath())).getParent()
							+ "\\", "UTF-8");
		} catch (UnsupportedEncodingException e)
		{
			System.err.println("Could not get program path: ");
			e.printStackTrace();
			return null;
		}
	}

	public static String loadFileAsString(String path)
	{
		return loadFileAsString(new File(path));
	}

	public static String loadFileAsString(File file)
	{
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader(file));
			StringBuilder res = new StringBuilder();
			String line = "";

			while ((line = reader.readLine()) != null)
			{
				res.append(line + "\n");
			}

			reader.close();

			return res.toString();
		} catch (IOException e)
		{
			System.err.println("Could not file as string: ");
			e.printStackTrace();
			return null;
		}
	}
}
