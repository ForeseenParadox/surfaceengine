package net.vitalrealms.engine.utils;

public class Delay
{

	private long delay;
	private long lastCheck;

	public Delay(long delay)
	{
		this.delay = delay;
		this.lastCheck = System.currentTimeMillis();
	}

	public long getDelay()
	{
		return delay;
	}

	public void setDelay(long delay)
	{
		this.delay = delay;
	}

	public boolean isReady()
	{

		if (System.currentTimeMillis() - lastCheck >= delay)
		{
			lastCheck = System.currentTimeMillis();
			return true;
		} else
		{
			return false;
		}

	}

}
